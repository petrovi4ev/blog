* Простой персональный блог.

- Установка `go get -u gitlab.com/petrovi4ev/blog`
- Переходим в рабочую директорию `cd $GOPATH/src/gitlab.com/petrovi4ev/blog`
- У нас должна работать база Postgres и должна быть создана БД restapi_dev
- У нас должна быть установлена утилита для проведения миграций, например golang-migrate/migrate, [инструкция по установке cli](https://github.com/golang-migrate/migrate/tree/master/cmd/migrate).
- Проводим миграции `migrate -database postgres://<пользователь БД>:<пароль>@<[хост (обычно localhost)]>/<название БД>?sslmode=disable -path migrations up`
- Собираем бинарник и запускаем `make && ./bin/apiserver`