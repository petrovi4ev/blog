module gitlab.com/petrovi4ev/blog

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gorilla/mux v1.7.4
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.5.0
	golang.org/x/sys v0.0.0-20200219091948-cb0a6d8edb6c // indirect
)
