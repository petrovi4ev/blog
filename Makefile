.PHONY: build
build:
	go build -o bin/apiserver ./cmd/apiserver

.PHONY: test
test:
	go test -v -race -timeout 30s ./...

.DEFAULT_GOAL := build

.PHONY: migrate
migrate:
	migrate -database postgres://default:secret@localhost/restapi_dev?sslmode=disable -path migrations up

.PHONY: run-local
run-local:
	go build -o bin/apiserver ./cmd/apiserver && ./bin/apiserver -config-path configs/apiserver.toml

.PHONY: run-heroku
run-heroku:
	git push heroku master && heroku open