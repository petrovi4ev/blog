package apiserver

import (
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/petrovi4ev/blog/internal/app/store"
)

// APIServer ...
type APIServer struct {
	config *Config
	logger *logrus.Logger
	router *mux.Router
	store  *store.Store
}

// New ...
func New(config *Config) *APIServer {
	return &APIServer{
		config: config,
		logger: logrus.New(),
		router: mux.NewRouter(),
	}
}

// Start ...
func (s *APIServer) Start() error {
	if err := s.configureLogger(); err != nil {
		return err
	}
	s.configureRouter()
	if err := s.configureStore(); err != nil {
		return err
	}
	port := ":" + os.Getenv("PORT")
	if len(port) <= 1 {
		port = s.config.BindAddr
	}
	s.logger.Info("starting api server! port", port)

	return http.ListenAndServe(port, s.router)
}

func (s *APIServer) configureLogger() error {
	level, err := logrus.ParseLevel(s.config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	s.logger.SetLevel(level)

	return nil
}

func (s *APIServer) configureStore() error {
	st := store.New(s.config.Store)
	if err := st.Open(); err != nil {
		return err
	}
	s.store = st

	return nil
}

func (s *APIServer) configureRouter() {
	s.router.HandleFunc("/", s.handleIndex())
}

func (s *APIServer) handleIndex() http.HandlerFunc {
	// здесь могут быть определения и логика которые будут необходимы только для этого хендлера. они выполнятся один раз.
	return func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hello it`s new blog...")
	}
}
