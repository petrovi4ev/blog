package apiserver

import "gitlab.com/petrovi4ev/blog/internal/app/store"

// Config ...
type Config struct {
	BindAddr string `toml:"bind_addr"`
	LogLevel string `toml:"log_level"`
	Store *store.Config
}

// NewConfig ...
func NewConfig() *Config {
	return &Config{
		BindAddr: ":9000",
		LogLevel: "debug",
		Store: store.NewConfig(),
	}
}
